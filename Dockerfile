FROM docker.io/debian:stable-slim

EXPOSE 15777/udp
EXPOSE 15000/udp
EXPOSE 7777/udp

ADD entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh \
 && useradd -m -r -s /bin/sh -u 900 steam \
 && sed -i 's/main$/main contrib non-free non-free-firmware/g' /etc/apt/sources.list.d/debian.sources \
 && dpkg --add-architecture i386 \
 && apt-get update \
 && yes 2 | apt-get install -y steamcmd ca-certificates libsdl2-2.0-0:i386 \
 && mkdir -p /home/steam/.config/Epic/FactoryGame/Saved /gamefiles /savegames \
 && chown -R 900:900 /home/steam/.config /gamefiles /savegames \
 && ln -sf /savegames /home/steam/.config/Epic/FactoryGame/Saved/SaveGames

VOLUME /gamefiles
VOLUME /savegames

USER 900:900

CMD ["/entrypoint.sh"]
